const express = require("express");
const router = express.Router();
const mySQL = require("../config/config-MySQL");

exports.getProductByName = router.get("/api/product/telkom", (req, res) => {
  mySQL.query(
    "SELECT * FROM `product-telkom` ORDER BY `product_name`",
    (err, results) => {
      if (err) console.log(err);
      res.send(JSON.stringify(results));
    }
  );
});

exports.putProduct = router.put(
  "/api/product/telkom-update/:id",
  (req, res) => {
    console.log(req.body)
    mySQL.query(
      //perhatikan penulisan tanda baca pada query, kurang atau kelebihan tanda koma
      //maka akan dapat menyebabkan syntax error, atau kurang field
      "UPDATE `product-telkom` SET `product_category`=?, `product_name`=?, `product_size`=?, `product_color`=?, `product_price`=? WHERE product_id=?",
      [
        req.body.product_category,
        req.body.product_name,
        req.body.product_size,
        req.body.product_color,
        req.body.product_price,
        // req.body.date_added, tidak perlu memasukkan tanggal di sini dan query, karena tidak perlu update
        req.params.id
      ],
      (err, results, fields) => {
        if (err) console.log(err);
        res.send(JSON.stringify(results));
      }
    );
  }
);

exports.postProduct = router.post(
  "/api/product/telkom-new",
  (req, res, next) => {
    const postData = req.body;
    // console.log(postData)
    mySQL.query(
      "INSERT INTO `product-telkom` SET ?",
      postData,
      (err, results, fields) => {
        if (err) console.log(err);
        res.send({
          code: 200,
          status: "data berhasil ditambah"
          // message: JSON.stringify(results)
        });
      }
    );
  }
);

exports.deleteProduct = router.delete(
  "/api/product/telkom-delete/:id",
  (req, res) => {
    const conDeleteData = { id: req.params.id };
    const idToDelete = req.params.id;
    mySQL.query(
      "DELETE FROM `product-telkom` WHERE product_id=?",
      [idToDelete, conDeleteData],
      (err, results, fields) => {
        if (err) console.log(err);
        res.end(JSON.stringify(results));
      }
    );
  }
);

module.exports = router;
