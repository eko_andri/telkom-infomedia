const columns = [
  {
    title: "Merk Produk",
    dataIndex: "product_name",
    key: "product_name",
    filters: productNameFilter,
    filteredValue: filteredInfo.product_name || null,
    onFilter: (value, record) => record.product_name.includes(value),
    sorter: (a, b) => a.product_name.length - b.product_name.length,
    sortOrder: sortedInfo.columnKey === "product_name" && sortedInfo.order
  },
  {
    title: "Kategori",
    dataIndex: "product_category",
    key: "product_category",
    filters: categoryFilter,
    filteredValue: filteredInfo.product_category || null,
    onFilter: (value, record) => record.product_category.includes(value),
    sorter: (a, b) => a.product_category.length - b.product_category.length,
    sortOrder:
      sortedInfo.columnKey === "product_category" && sortedInfo.order
  },
  {
    title: "Ukuran",
    dataIndex: "product_size",
    key: "product_size",
    filters: sizeFilter,
    filteredValue: filteredInfo.product_size || null,
    onFilter: (value, record) => record.product_size.includes(value),
    sorter: (a, b) => a.product_size.length - b.product_size.length,
    sortOrder: sortedInfo.columnKey === "product_size" && sortedInfo.order
  }
];

export default columns;