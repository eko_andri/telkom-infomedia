import React from "react";

export default function Footer() {
  return (
    <div>
      <a
        href="https://subarnanto.com"
        target="_blank"
        rel="noopener noreferrer"
        className="footer"
      >
        Created by Eko Andri Subarnanto - Web Developer & Designer
      </a>
    </div>
  );
}
