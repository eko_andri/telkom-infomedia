import React, { useState } from "react";
import { Button } from "antd";
import FormIsiData from "./Function/FormIsiData";
import TableData from "./Function/TableData";

import Config from "../config/ConfigData";
import Header from "./Basic/Header";
import Footer from "./Basic/Footer";
import "./App.css";

// class App extends React.Component{
// state = {
//   URL: process.env.NODE_ENV === "production" ? Config.prodURL : Config.devURL,
//   route: "app"
// };

function App() {
  const [URL] = useState(
    process.env.NODE_ENV === "production" ? Config.prodURL : Config.devURL
  );
  const [route, setRoute] = useState("app");

  function handleForm(route) {
    if (route === "form") {
      setRoute("form");
    } else if (route === "app") {
      setRoute("app");
    }
    setRoute(route);
  }

  // render() {
  //   const { URL, route } = this.state;

  return (
    <div>
      <Header />
      <div className="table-operations">
        <Button onClick={() => handleForm("form")}>Form Isi Data</Button>
        <Button onClick={() => handleForm("app")}>Lihat Tabel Data</Button>
      </div>
      {route === "form" ? <FormIsiData URL={URL} /> : <TableData URL={URL} />}
      <Footer />
    </div>
  );
  // }
}

export default App;
