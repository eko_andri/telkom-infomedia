import React, { useEffect, useState } from "react";
import { Table, Button, Popconfirm } from "antd";
import { success, error } from "../Basic/InformationModal";
import { EditableCell, EditableContext, EditableFormRow } from "./Editable";

import {
  getProductName,
  getProductNameFilter,
  getCategoryFilter,
  getSizeFilter,
  getColorFilter
} from "../Crud/GetData";

import { putData, deleteData } from "../Crud/CUDData";

// export default class TableData extends React.Component {
//   state = {
//     filteredInfo: null,
//     sortedInfo: null,
//     dataProduct: [],
//     productNameFilter: [],
//     categoryFilter: [],
//     sizeFilter: [],
//     colorFilter: [],
//     editingKey: "",
//     componentUpdate: false
//   };
function TableData(){
  // 1. Use the name state variable
  // const [name, setName] = useState('Mary');

  // // 2. Use an effect for persisting the form
  // useEffect(function persistForm() {
  //   localStorage.setItem('formData', name);
  // });

  // // 3. Use the surname state variable
  // const [surname, setSurname] = useState('Poppins');

  // // 4. Use an effect for updating the title
  // useEffect(function updateTitle() {
  //   document.title = name + ' ' + surname;
  // });

const [filteredInfo, useFilteredInfo] = useState(null)
const [sortedInfo, useSortedInfo] = useState(null)

  componentDidMount() {
    const { URL } = this.props;
    getProductName(URL).then(response => {
      this.setState({ dataProduct: response.data });
    });

    getProductNameFilter(URL).then(result => {
      this.setState({
        productNameFilter: result
      });
    });

    getCategoryFilter(URL).then(result => {
      this.setState({
        categoryFilter: result
      });
    });

    getSizeFilter(URL).then(result => {
      this.setState({
        sizeFilter: result
      });
    });

    getColorFilter(URL).then(result => {
      this.setState({
        colorFilter: result
      });
    });
  }

  handleForm = route => {
    if (route === "form") {
      this.setState({ route: "form" });
    } else if (route === "app") {
      this.setState({ route: "app" });
    }
    this.setState({ route });
  };

  handleChange = (pagination, filters, sorter) => {
    // console.log("Various parameters", pagination, filters, sorter);
    this.setState({
      filteredInfo: filters,
      sortedInfo: sorter
    });
  };

  clearFilters = () => {
    this.setState({ filteredInfo: null });
  };

  clearAll = () => {
    this.setState({
      filteredInfo: null,
      sortedInfo: null
    });
  };

  setProductSort = () => {
    this.setState({
      sortedInfo: {
        order: "descend",
        columnKey: "product_category"
      }
    });
  };

  isEditing = record => record.product_id === this.state.editingKey;

  cancel = () => {
    this.setState({ editingKey: "" });
  };

  save(form, product_id) {
    form.validateFields((err, row) => {
      if (err) {
        error("Error", "Cek kembali masukan Anda");
        return;
      }
      const newData = [...this.state.dataProduct];
      const index = newData.findIndex(item => product_id === item.product_id);
      if (index > -1) {
        const item = newData[index];
        newData.splice(index, 1, {
          ...item,
          ...row
        });
        putData(URL, product_id, newData, index);
        this.setState({
          dataProduct: newData,
          editingKey: "",
          componentUpdate: true
        });
        success("Sukses", "Anda berhasil mengubah data");
      } else {
        newData.push(row);
        this.setState({ dataProduct: newData, editingKey: "" });
      }
    });
  }

  edit(product_id) {
    // product_id adalah primary key dalam table yang akan diedit
    // console.log(product_id);
    this.setState({ editingKey: product_id });
  }

  handleDelete = product_id => {
    deleteData(URL, product_id);
    success(
      "Sukses",
      "Anda berhasil menghapus data, refresh page ini untuk melihat hasilnya"
    );
  };

  // render() {
  //   const {
  //     dataProduct,
  //     productNameFilter,
  //     categoryFilter,
  //     sizeFilter,
  //     colorFilter
  //   } = this.state;

    let { sortedInfo, filteredInfo } = this.state;
    sortedInfo = sortedInfo || {};
    filteredInfo = filteredInfo || {};
    const columns = [
      {
        title: "Merk Produk",
        dataIndex: "product_name",
        key: "product_name",
        filters: productNameFilter,
        filteredValue: filteredInfo.product_name || null,
        onFilter: (value, record) => record.product_name.includes(value),
        sorter: (a, b) => a.product_name.length - b.product_name.length,
        sortOrder: sortedInfo.columnKey === "product_name" && sortedInfo.order
      },
      {
        title: "Kategori",
        dataIndex: "product_category",
        key: "product_category",
        editable: true,
        filters: categoryFilter,
        filteredValue: filteredInfo.product_category || null,
        onFilter: (value, record) => record.product_category.includes(value),
        sorter: (a, b) => a.product_category.length - b.product_category.length,
        sortOrder:
          sortedInfo.columnKey === "product_category" && sortedInfo.order
      },
      {
        title: "Ukuran",
        dataIndex: "product_size",
        key: "product_size",
        editable: true,
        filters: sizeFilter,
        filteredValue: filteredInfo.product_size || null,
        onFilter: (value, record) => record.product_size.includes(value),
        sorter: (a, b) => a.product_size.length - b.product_size.length,
        sortOrder: sortedInfo.columnKey === "product_size" && sortedInfo.order
      },
      {
        title: "Warna",
        dataIndex: "product_color",
        key: "product_color",
        editable: true,
        filters: colorFilter,
        filteredValue: filteredInfo.product_color || null,
        onFilter: (value, record) => record.product_color.includes(value),
        sorter: (a, b) => a.product_color.length - b.product_color.length,
        sortOrder: sortedInfo.columnKey === "product_color" && sortedInfo.order
      },
      {
        title: "Harga",
        dataIndex: "product_price",
        key: "product_price",
        editable: true,
        sorter: (a, b) => a.product_price.length - b.product_price.length,
        sortOrder: sortedInfo.columnKey === "product_price" && sortedInfo.order
      },
      {
        title: "Aksi",
        dataIndex: "operation",
        render: (text, record) => {
          const editable = this.isEditing(record);
          return (
            <div>
              {editable ? (
                <span>
                  <EditableContext.Consumer>
                    {form => (
                      <span
                        className="save-button"
                        // href="javascript:;"
                        onClick={() => this.save(form, record.product_id)}
                        style={{ marginRight: 8 }}
                      >
                        Save
                      </span>
                    )}
                  </EditableContext.Consumer>
                  <Popconfirm
                    title="Sure to cancel?"
                    onConfirm={() => this.cancel(record.product_id)}
                  >
                    <span className="cancel-button">Cancel</span>
                  </Popconfirm>
                </span>
              ) : (
                <span
                  className="edit-button"
                  onClick={() => this.edit(record.product_id)}
                >
                  Edit
                </span>
              )}

              <span
                className="delete-button"
                onClick={() => this.handleDelete(record.product_id)}
              >
                Delete
              </span>
            </div>
          );
        }
      }
    ];

    const components = {
      body: {
        row: EditableFormRow,
        cell: EditableCell
      }
    };

    const dataColumns = columns.map(col => {
      if (!col.editable) {
        return col;
      }
      return {
        ...col,
        onCell: record => ({
          record,
          inputType: col.dataIndex === "product_price" ? "number" : "text",
          dataIndex: col.dataIndex,
          title: col.title,
          editing: this.isEditing(record)
        })
      };
    });

    return (
      <div>
        <div className="table-operations">
          <Button onClick={this.setProductSort}>Sort Kategori</Button>
          <Button onClick={this.clearFilters}>Clear filters</Button>
          <Button onClick={this.clearAll}>Clear filters and sorters</Button>
        </div>
        <Table
          // Warning: Each record in table should have a unique `key` prop,
          // or set `rowKey` to an unique primary key.
          rowKey="product_id" //to prevent error above
          components={components}
          bordered
          columns={dataColumns}
          dataSource={dataProduct}
          onChange={this.handleChange}
        />
      </div>
    );
  }

  componentDidUpdate(prevProps, prevState) {
    const { URL } = this.props;
    // console.log(URL);
    if (this.state.componentUpdate !== prevState.componentUpdate) {
      getProductName(URL).then(response => {
        this.setState({ dataProduct: response.data });
      });

      getProductNameFilter(URL).then(result => {
        this.setState({
          productNameFilter: result
        });
      });

      getCategoryFilter(URL).then(result => {
        this.setState({
          categoryFilter: result
        });
      });

      getSizeFilter(URL).then(result => {
        this.setState({
          sizeFilter: result
        });
      });

      getColorFilter(URL).then(result => {
        this.setState({
          colorFilter: result
        });
      });
    }
  // }
}

export default TableData;