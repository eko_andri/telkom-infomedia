import axios from "axios";

export function getProductName(URL) {
  return axios.get(URL + "api/product/telkom");
}

export function getProductNameFilter(URL) {
  return axios.get(URL + "api/product/telkom").then(response => {
    const dataTelkom = response.data;
    const filter = [];
    let obj = [];
    dataTelkom.map(data => {
      obj = {
        text: data.product_name,
        value: data.product_name
      };
      filter.push(obj);
      return null;
    });
    var result = filter.reduce((unique, o) => {
      if (!unique.some(obj => obj.text === o.text && obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    }, []);
    return result;
  });
}

export function getCategoryFilter(URL) {
  return axios.get(URL + "api/product/telkom").then(response => {
    const dataTelkom = response.data;
    const filter = [];
    let obj = [];
    dataTelkom.map(data => {
      obj = {
        text: data.product_category,
        value: data.product_category
      };
      filter.push(obj);
      return null;
    });
    var result = filter.reduce((unique, o) => {
      if (!unique.some(obj => obj.text === o.text && obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    }, []);
    return result;
  });
}

export function getSizeFilter(URL) {
  return axios.get(URL + "api/product/telkom").then(response => {
    const dataTelkom = response.data;
    const filter = [];
    let obj = [];
    dataTelkom.map(data => {
      obj = {
        text: data.product_size,
        value: data.product_size
      };
      filter.push(obj);
      return null;
    });
    var result = filter.reduce((unique, o) => {
      if (!unique.some(obj => obj.text === o.text && obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    }, []);
    return result;
  });
}

export function getColorFilter(URL) {
  return axios.get(URL + "api/product/telkom").then(response => {
    const dataTelkom = response.data;
    const filter = [];
    let obj = [];
    dataTelkom.map(data => {
      obj = {
        text: data.product_color,
        value: data.product_color
      };
      filter.push(obj);
      return null;
    });
    var result = filter.reduce((unique, o) => {
      if (!unique.some(obj => obj.text === o.text && obj.value === o.value)) {
        unique.push(o);
      }
      return unique;
    }, []);
    return result;
  });
}
