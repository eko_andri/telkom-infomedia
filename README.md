# telkom-infomedia

## [Demo Application](https://demo-ssr.subarnanto.com/)

Rest API for Product and Category

A simple table with filter and sort function

Clone this repo.

There are 2 folders, one for server and another one for client.

Server: cd to the server folder, run yarn or npm install.
Once it finished downloading, run yarn start.
This nodejs server is connected to mysql database using xampp.
You must have the database installed with the same configuration as in config folder inside the server folder.
The sample table is provided.

Client: cd to the client/infomedia folder, run yarn or npm install.
Once it finished downloading, run yarn start.
